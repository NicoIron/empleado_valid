package com.ejercicio.springboot.backend.nicolay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBackendNicolayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBackendNicolayApplication.class, args);
	}

}
