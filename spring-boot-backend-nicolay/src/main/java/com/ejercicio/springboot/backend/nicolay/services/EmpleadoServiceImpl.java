package com.ejercicio.springboot.backend.nicolay.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ejercicio.springboot.backend.nicolay.models.dao.IEmpleadoDao;
import com.ejercicio.springboot.backend.nicolay.models.entity.Empleado;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService{
	
	@Autowired
	private IEmpleadoDao empleadoDao;

	@Override
	@Transactional(readOnly = true)
	public List<Empleado> findAll() {
		return empleadoDao.findAll();
	}

	@Override
	public Empleado save(Empleado empleado) {
		return empleadoDao.saveAndFlush(empleado);
	}

	@Override
	public Empleado updateEmpleado(Long id, Empleado empleado) {
		return empleadoDao.findById(id)
				.map(data -> {
					data.setProcesado(empleado.isProcesado());
					return empleadoDao.saveAndFlush(data);
				}).orElse(null);
	}
	
	

}
