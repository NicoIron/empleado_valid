package com.ejercicio.springboot.backend.nicolay.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ejercicio.springboot.backend.nicolay.models.entity.Empleado;
import com.ejercicio.springboot.backend.nicolay.services.IEmpleadoService;

@RestController
@RequestMapping("/empleados")
public class EmpleadoRestController {
	
	@Autowired IEmpleadoService empleadoService;
	
	//Crear
	@PostMapping("/create")
	public Empleado crearEmpleado(@RequestBody Empleado empleado) {
		return empleadoService.save(empleado);
	}
	
	//Consultar
	@GetMapping
	public List<Empleado> index(){
		return empleadoService.findAll();
	}
	
	//Actualizar
	@PutMapping("/{id}")
	public Empleado actualizaEmpleado(@PathVariable Long id) {
		Empleado empleado = new Empleado();
		empleado.setProcesado(true);
		
		return empleadoService.updateEmpleado(id, empleado);
	}

}
