package com.ejercicio.springboot.backend.nicolay.services;

import java.util.List;

import com.ejercicio.springboot.backend.nicolay.models.entity.Empleado;

public interface IEmpleadoService {
	
	public List<Empleado> findAll();
		
	public Empleado save(Empleado empleado);
	
	public Empleado updateEmpleado(Long id, Empleado empleado);

}
