package com.ejercicio.springboot.backend.nicolay.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ejercicio.springboot.backend.nicolay.models.entity.Empleado;

public interface IEmpleadoDao extends JpaRepository<Empleado, Long>{

}
